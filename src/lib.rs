#![no_std]
//! Provides streaming and complete [nom] parsers for the
//! [ABNF Core Rules](https://datatracker.ietf.org/doc/html/rfc5234)
//! that are overridable, as well as providing shorthands to ease the translation of ABNF
//! rule specifications into [nom] parser/combinator functions.
//!
//! Note that these rules are very ascii oriented - this module implements them exactly as
//! written, in a byte-oriented format. Luckily UTF-8 is kind to ASCII text by setting the
//! first bit in each byte as 1 so it does not interfere with ASCII parsers.
//!
//! Basically, I'm saying use string references with this and you'll be fine ^.^
//!
//! # BASICS
//! See [nom_byte_utils] and it's [nom_byte_utils::complete], [nom_byte_utils::streaming]
//! submodules for some very primitive utility functions (for now just the one) to enable simple
//! [nom] parsing of byte streams using a similar technique as [nom::characters::satisfy]
//!
//! For implementations of the ABNF core rules as [nom] parser functions, see the traits
//! [complete::ABNFCoreRules] and [streaming::ABNFCoreRules], as well as implementors of
//! these traits that simply *use* the defaults [complete::DefaultABNFCoreRules] and
//! [streaming::DefaultABNFCoreRules]. The rules are a series of functions r_<rule name in
//! lowercase> on the traits.
//!
//! # OVERRIDING DEFAULT RULES
//! Sometimes, you end up with a specification that uses a slightly modified version of the ABNF
//! Core Rules, and you need to override them. This crate provides a simple, modular mechanism to
//! do so - via the traits [complete::ABNFCoreRules] and [streaming::ABNFCoreRules].
//!
//! Users of these rules - that is, people defining sets of rules for other formats using these
//! rules - should take an implementor of [complete::ABNFCoreRules] or [streaming::ABNFCoreRules]
//! as a type argument, rather than just pulling the rules from [complete::DefaultABNFCoreRules]
//! and [streaming::DefaultABNFCoreRules] (if you're worried about usability, just set the defaults
//! needed for the format to one of these or a local overriding implementation).
//!
//! That way, people using your set of rules can plug in their own core rules for special cases
//! when necessary. This method is actually completely extensible as well, if people writing
//! collections of rules themselves do it as a trait and provide default, overridable
//! implementations.
//!
//! ## Example
//! Lets say you want to parse an arbitrary stream of ABNF hexdigits in your unique file or packet
//! format. You write a trait with the following rules as a convenient collection:
//! ```rust
//! use nom_abnf_core_rules::complete::{ABNFCoreRules, DefaultABNFCoreRules};
//! use nom::{error::ParseError, multi::fold_many_m_n, combinator::recognize};
//!
//! pub trait MyHexParser<ABNFCore: ABNFCoreRules = DefaultABNFCoreRules> {#
//!     /// Rule for parsing 128 bit hex chunks
//!     /// Hypothetically here you could include stuff for parsing hex digits into a number,
//!     /// or something else like that.
//!     fn be_hex_chunk<'i, E: ParseError<&'i[u8]>>(input: &'i[u8])
//!         -> nom::IResult<&'i[u8], u128, E> {
//!         let (i, read) = recognize(fold_many_m_n(
//!             32, 32,
//!             ABNFCore::r_hexdigit,
//!             // Weird stuff to do nothing basically
//!             || (), |_, _|()
//!         ))(i)?;
//!         Ok((i, be_u128_hexdigits(read)))
//!
//!     }
//! }
//!
//! // Note that this is NOT a good way to map hex when you expect varying rules at all.
//! // You should use some translator included in your rules.
//! pub fn be_u128_hexdigits(v: &[u8]) -> u128 {
//!     let vlen = v.len();
//!     let res = v.iter()
//!         .enumerate()
//!         // Use index version rather than shift-modify to reduce data dependence
//!         .fold(0u128, |acc, (hex_index, character)| {
//!             acc | (match character {
//!                 b'0' => 0,
//!                 b'1' => 1,
//!                 b'2' => 2,
//!                 b'3' => 3,
//!                 b'4' => 4,
//!                 b'5' => 5,
//!                 b'6' => 6,
//!                 b'7' => 7,
//!                 b'8' => 8,
//!                 b'9' => 9,
//!                 b'A' | b'a' => 10,
//!                 b'B' | b'b' => 11,
//!                 b'C' | b'c' => 12,
//!                 b'D' | b'd' => 13,
//!                 b'E' | b'e' => 14,
//!                 b'F' | b'f' => 15,
//!                 _ => panic!("Input must be filtered")
//!             } << 4 * (vlen - hex_index))
//!         });
//!     res
//! }
//!
//! ```

// Note for future readers (me or anyone else) and just random people ^.^ nya
//
// https://docs.rs/nom/7.1.0/src/nom/multi/mod.rs.html#786-832 may have an optimisation at the end
// in the case where a streaming parser returns Needed with an actual amount.
//
// Since fold_while_m_n (and presumably the others) are greedy and only stop in a streaming parser
// when the next part actually fails, it is better to indicate an unknown needed size so that people
// stream-parsing won't have to repeatedly reparse things due to only requesting a minimum of extra
// bytes, only to then go through a fold_while_m_n parse a bajillion times as more IO requests or
// other stream source requests are made.
//
// Worst case I can see is a request time of O((n - m) * ( O(inner parser) + O(io transfer) ) ),
// as opposed to the best which is a simple O((n - m)*O(inner parser)  + O(io_transfer)) nya, Luckily,
// it seems that amplification attacks are not so possible, since an outer fold_while_m_n will get
// success from each front input as more bytes are pulled.

/// Contains some extra utilities for ease of pure byte-input nom parsers
pub mod nom_byte_utils {
    pub mod complete {
        /// Generic combinator for matching exactly 1 item from an input that lets you
        /// [nom::bytes::complete::take] - despite the name, it can take any input with
        /// certain properties
        pub fn single_input_matches<
            Item,
            I: nom::InputIter<Item = Item> + nom::InputTake + Clone,
            E: nom::error::ParseError<I>,
        >(
            matcher: impl Fn(&Item) -> bool,
        ) -> impl FnMut(I) -> nom::IResult<I, I, E> {
            nom::combinator::verify(nom::bytes::complete::take(1usize), move |v: &I| {
                matcher(&v.iter_elements().next().expect("Already ensured > 0"))
            })
        }
    }

    pub mod streaming {
        /// Generic combinator for matching exactly 1 item from an input that lets you
        /// [nom::bytes::streaming::take] - despite name, it can take any input with
        /// certain properties
        pub fn single_input_matches<
            Item,
            I: nom::InputIter<Item = Item> + nom::InputTake + Clone + nom::InputLength,
            E: nom::error::ParseError<I>,
        >(
            matcher: impl Fn(&Item) -> bool,
        ) -> impl FnMut(I) -> nom::IResult<I, I, E> {
            nom::combinator::verify(nom::bytes::streaming::take(1usize), move |v: &I| {
                matcher(&v.iter_elements().next().expect("Already ensured > 0"))
            })
        }
    }
}

/// Macro for checking ranges (including boundary points) of values in a type en-masse
/// for the purpose of constructing character classes. Also allows optional manual type 
/// specification
///
/// # SYNTAX
///
/// ranges(
///     <variable-identifier> in <[optional-type]>
///     [lower-bound-literal] ~ [upper-bound-literal],...
/// )
///
/// # EXAMPLES
///
/// Hex digit identifier
/// ```rust
/// use nom_abnf_core_rules::ranges;
///
/// pub const fn is_hex_digit(chbyte: u8) -> bool {
///     ranges!(chbyte in b'A' ~ b'Z', b'a' ~ b'z', b'0' ~ b'9')
/// }
///
/// assert!(is_hex_digit(b'5'))
/// assert!(!is_hex_digit(b'q'))
/// assert!(is_hex_digit(b'a'))
/// ```
///
/// ASCII identifier
/// ```rust
/// use nom_abnf_core_rules::ranges;
///
/// pub const fn is_ascii_byte(chbyte: u8) -> bool {
///     ranges!(chbyte in ~ 0x7F)
/// }
///
/// assert!(is_ascii_byte(b'a'))
/// assert!(!is_ascii_byte(0x80))
/// assert!(is_ascii_byte(0x7F))
/// ```
#[macro_export]
macro_rules! ranges { 
    ($var:ident in $($($lbinc:literal)? ~ $($ubinc:literal)?),*) => {{
        $(ranges!(@ $var in $($lbinc)? ~ $($ubinc)?))||*
    }};
    ($var:ident in <$type:ty> $($($lbinc:literal)? ~ $($ubinc:literal)?),*) => {{
        $(ranges!(@ $var in <$type> $($lbinc)? ~ $($ubinc)?))||*
    }};
    (@ $var:ident in $(<$type:ty>)? ~ ) => { (true) };
    (@ $var:ident in $(<$type:ty>)? $lbinc:literal ~) => { ($lbinc $(as $type)? <= $var) };
    (@ $var:ident in $(<$type:ty>)? ~ $ubinc:literal) => { ($var <= $ubinc $(as $type)?) };
    (@ $var:ident in $(<$type:ty>)? $lbinc:literal ~ $ubinc:literal) => { 
        ($lbinc $(as $type)? <= $var && $var <= $ubinc $(as $type)?) 
    };
}


#[cfg(test)]
mod ranges_tests {

    fn is_ascii_byte(b: u8) -> bool {
        ranges!(b in <u8> ~ 0x7F)
    }

    #[test]
    fn ranges_upper_bound_only() {
        assert!(is_ascii_byte(0));
        assert!(is_ascii_byte(0x7F));
        assert!(!is_ascii_byte(0x80));
        assert!(is_ascii_byte(b'a'));
    }

    fn is_unicode_byte(b: u8) -> bool {
        ranges!(b in 0x80 ~ )
    }

    #[test]
    fn ranges_lower_bound_only() {
        for byte in 0..=255u8 {
            assert_ne!(is_ascii_byte(byte), is_unicode_byte(byte));
        }
    }

    /// Don't use this in real implementations
    fn is_capital_ascii(b: u8) -> bool {
        ranges!(b in b'A' ~ b'Z')
    } 

    /// Don't use this in real implementations.
    fn is_alphabetical_ascii(b: u8) -> bool {
        ranges!(b in b'A' ~ b'Z', b'a' ~ b'z')
    }

    #[test]
    fn ranges_both_ends() {
        for byte in b'A'..=b'Z' {
            assert!(is_capital_ascii(byte))
        }
        for byte in (0..b'A').into_iter().chain((b'Z' + 1)..u8::MAX) {
            assert!(!is_capital_ascii(byte))
        }
    }

    #[test]
    fn multi_ranges() {
        for byte in (b'A'..=b'Z').into_iter().chain((b'a'..=b'z').into_iter()) {
            assert!(is_alphabetical_ascii(byte))
        }
        for byte in (0..b'A').into_iter().chain(((b'Z' + 1)..b'a').into_iter()).chain(((b'z' + 1)..=u8::MAX).into_iter()) {
            assert!(!is_alphabetical_ascii(byte))
        } 
    }
}


/// Macro for performing efficient character translations🏳‍⚧️//numerical translations using matches.
///
/// This macro is designed to be a very powerful method of translating regions of a type's valid
/// space (typically characters) into another region. It also relies on debug assertions (and compiler warnings!) 
/// to ensure that the provided ranges are matching in size, when the upper bounds or lower bounds
/// are specified in the output.
///
///
/// ## SOURCE AND TARGET RANGES
/// When a list of source ranges is provided, an input value is checked for membership in each one
/// sequentially (with the first matching translation provided being used). All ranges include
/// their endpoints.
///
/// The output range is known as the target range, and it can either be in up mode (default)
/// or down mode (up and down modes can be set explicitly with 'up' and 'down' placed in front of
/// the target range).
///
/// The default (up mode) maps the first character/number in the source range to the 
/// first character/number in target range, with each value beyond that in the source range
/// corresponding to the next value in the target range as well.
///
/// Down mode, however, maps the first character/number in the source range to the last element in
/// the target range, with each increase in the value matching the source range corresponding to a 
/// decrease in value emitted from the target range.
///
/// In up mode, target ranges can be specified only with their lower bound (however, it is not 
/// recommended as the upper bound allows extra checks). Not providing the *lower bound* will
/// result in it being set to 0.
///
/// In down mode, target ranges can be specified only with their upper bound (however, it is not
/// recommended as the lower bound allows extra checks). Not providing the upper bound will result
/// in it essentially being treated as value_type::MAX
///
///
///
///
/// ## Custom numeric types
/// If you have custom numeric types, you need to ensure they have a ::MAX and ::MIN type 
/// constants like all of the standard rust types, as well as a + and - operator, >, >=, <, <=
/// comparators too.
/// 
/// 
#[macro_export]
macro_rules! translations {
    ($val:expr => <$sourcetype:ty => $targettype:ty> $(
        $($lowerbound:literal)? => $($upperbound:literal)?
        trans $($up_or_down:ident)? $($lowerout:literal)? => $($upperout:literal)?
        $(overunder $overunderflow:ident)?
    ),*
    $(failmode $failmode:ident)? 
    ) => {{
        // Macro weirdness here because having a ? inside a * that is defined outside 
        // the * does not work nya
        translations!(
            @failmode $($failmode)? 
            @match $val {$( 
                v if $crate::ranges!(v in <$sourcetype> $($lowerbound)? ~ $($upperbound)?)
                    => ::core::result::Result::Ok({
                    // Translation can go in one of two directions
                    // up or down
                    //
                    // Up translation means the source range increasing means the output is 
                    // increasing - it is the default.
                    //
                    // Down translation means the source range increasing causes the output 
                    // range to decrease
                    //
                    // Note that we use *wrapping_sub* and *wrapping_add* for calculations of the
                    // change to avoid serious annoyances - the final value is in fact 
                    translations!(@calcfunction <$sourcetype => $targettype> 
                        $($up_or_down)? @ $($overunderflow)? @
                        translations!(@outshift <$sourcetype> $($lowerbound)? => v $(ub $upperbound)?);
                        translations!(@targetexpression <$targettype> @ $($lowerout)? => $($upperout)?) 
                    ) 
                }),)*
                v @ _ => ::core::result::Result::Err(v)
            }
        )
    }};

    (@failmode option @ $val:expr) => {(
        $val.as_some()
    )};

    (@failmode result @ $val:expr) => {(
        $val
    )};
    
    // Panic mode (default)
    (@failmode panic @ $val:expr) => {( 
        $val.expect("Numerical value not in map") 
    )};
    
    (@failmode @ $val:expr) => {( translations!(@failmode panic @ $val) )};

    // The amount to add to the lower output bound or subtract from the upper output 
    // bound when in up or down mode, respectively nya. Because lower bounds are <= upper bounds,
    // and inputs are selected based on this, we know that the input is >= lower bound.
    //
    // This means we also don't need any fancy wrapping_add/wrapping_sub because we know it will
    // NEVER wrap
    (@outshift <$st:ty> $lowerbound:expr => $inval:ident $(ub $ub:literal)?) => {
        ($inval - $lowerbound)
    };
    // No lower bound provided, use sourcetype::min.
    (@outshift <$st:ty> => $inval:ident $(ub $ub:literal)?) => {
        translations!(@outshift <$st>::MIN => $inval $(ub $ub)?)
    };
    
    // Default when there's NOTHING AT ALL nyaaa
    //
    // Arguments to this submacro are the expression derived from source-type components
    // and expressions derived from the target type components.
    (@calcfunction <$st:ty => $tt:ty> @ @ $src:expr ; $target:expr) => {
          translations!(@calcfunction <$st => $tt> up @ plain @ $src ; $target)
    };
    // Default direction - up or down
    (@calcfunction <$st:ty => $tt:ty> @ $overflow_option:ident @ $src:expr ; $target:expr) => {
        translations!(@calcfunction <$st => $tt> up @ $overflow_option @ $src ; $target)
    };
    // Shift calculation functions - dependent on wrapping, saturating, or default.
    (@calcfunction <$st:ty => $tt:ty> $direction:ident @ @ $src:expr ; $target:expr) => {
        translations!(@calcfunction <$st => $tt> $direction plain @ $src ; $target)
    };

    // Plain/normal
    (@calcfunction <$st:ty => $tt:ty> up @ plain @ $src:expr ; $target:expr) => {
        ($target + $src)
    };
    (@calcfunction <$st:ty => $tt:ty> down @ plain @ $src:expr ; $target:expr) => {
        ($target - $src)
    };
    // Wrapping
    (@calcfunction <$st:ty => $tt:ty> up @ wrapping @ $src:expr ; $target:expr) => {
        ($tt::wrapping_add($target, ::core::convert::Into<$src) )
        (::core::convert::Into<<$target>>::into($src).wrapping_add($target))
    };
    (@calcfunction <$st:ty => $tt:ty> down @ wrapping @ $src:expr ; $target:expr) => {
        (::core::convert::Into<<$target>>::into($src).wrapping_sub($target))
    };
    // Saturating
    (@calcfunction <$st:ty => $tt:ty> up @ saturating @ $src:expr ; $target:expr) => {
        (::core::convert::Into<<$target>>::into($src).saturating_add($target))
    };
    (@calcfunction <$st:ty => $tt:ty> down @ saturating @ $src:expr ; $target:expr) => {
        (::core::convert::Into<<$target>>::into($src).saturating_sub($target))
    };

    // Obtains the expression for the target type from the upper and lower bounds, with 
    // associated defaults
    //
    // First part indicates default direction
    (@targetexpression <$tt:ty> @ $($lo:expr)? => $($uo:expr)?) => {
        translations!(@targetexpression <$tt> up @ $($lo)? => $($uo)?)
    };
    // Next the defaults for upper-out-bounds (in the case of direction down) or
    // lower-out-bounds (in the case of direction up)
    (@targetexpression <$tt:ty> up @ => $($uo:expr)?) => {
        translations!(@targetexpression <$tt> up @ 0 => $($uo)?)
    };
    (@targetexpression <$tt:ty> down @ $($lo:expr)? => ) => {
        translations!(@targetexpression <$tt> down @ $($lo)?; <$tt>::MAX)
    };
    // Now for actual values nyaaaa
    // Note the expr here is because we might have obtained a ::MAX further up ;p
    //
    // This produces the expression from the target range that should be presented.
    // For UP mode, we want the lower output bound which is getting added on to nya
    // For DOWN mode, we want the upper output bound which is getting subtracted from.
    (@targetexpression <$tt:ty> up @ $lo:expr => $uo:expr) => { $lo };
    (@targetexpression <$tt:ty> down @ $lo:expr => $uo:expr) => { $uo };

    // Debug expressions time! nya
    //
    // Note that in the case of missing source range bounds, we still do checks by using the
    // defaults.
    //
    // In the case of the target range missing anything, do nothing.
    (@debugrange <$src:ty => $tgt:ty> (=> $($ub:expr)?) => $lo:literal ~ $uo:literal) => {
        translations!(@debugrange <$src => $tgt> ($src::MIN => $($ub)?) => $lo ~ $uo)
    };
    (@debugrange <$src:ty => $tgt:ty> ($lb:expr =>) => $lo:literal ~ $uo:literal) => { 
        translations!(@debugrange <$src => $tgt> ($lb => $tgt::MAX) => $lo ~ $uo)
    }; 
    (@debugrange <$src:ty => $tgt:ty> ($lb:expr => $ub:expr) => $lo:literal ~ $uo:literal) => {
        // Check the range differences are zero
        ::core::macros::assert_eq!(($ub - $lb), ($uo - $lo));
    };
    // Last case - no or only one provided endpoint literals, do not try to debug assert.
    (@debugrange <$src:ty => $tgt:ty> ($($lb:expr)? => $($ub:expr)?) =>$($lo:literal)? ~ $($uo:literal)?) => {}; 
}

#[cfg(test)]
mod translations_test {
    fn bin_chars(chr: u8) -> u8 {
        translations!(
            chr => <u8 => u8> b'0' => b'1' trans 0u8 => 1u8
        )
    }

    fn hex_chars_lower(chr: u8) -> u8 {
        translations!(
            chr => <u8 => u8>
                b'0' => b'9' trans 0 => 9,
                b'a' => b'f' trans 10 => 15
        ) 
    }
    
    fn hex_chars_mixed_case(chr: u8) -> u8 {
        translations!(
            chr => <u8 => u8>
                b'0' => b'9' trans 0 => 9,
                b'a' => b'f' trans 10 => 15,
                b'A' => b'F' trans 10 => 15
        ) 
    }

    #[test]
    #[should_panic]
    /// Test that the default error mode works.
    fn out_of_range_panic() {
        bin_chars(b'4');
    }

    #[test]
    /// Testing primitive functionality
    fn basic_single_functionality() {
        for b in 0..1 {
            assert_eq!(bin_chars(b'0' + b), b);
        }
    }

    #[test]
    /// Testing translations with multiple ranges available
    fn multi_ranges() {
        // First range nya (noninclusive
        for h in 0..10 {
            assert_eq!(hex_chars_lower(b'0' + h), h);
        }
        // Second range ^.^
        for h in 10..16 {
            assert_eq!(hex_chars_lower(b'a' + h - 10), h);
        }
    }

    #[test]
    /// Overlapping output ranges
    fn overlapping_out_ranges() {
        // First range nya (noninclusive
        for h in 0..10 {
            assert_eq!(hex_chars_mixed_case(b'0' + h), h);
        }
        // Second range ^.^
        for h in 10..16 {
            assert_eq!(hex_chars_mixed_case(b'a' + h - 10), h);
        }
        
        // Third range ^.^
        for h in 10..16 {
            assert_eq!(hex_chars_mixed_case(b'A' + h - 10), h);
        }
    }

    #[test]
    /// Overlapping input ranges - should have the first range listed be the first range used
    fn overlapping_input_ranges() {
        let trans = |v| translations!(
            v => <u8 => u8> 
                0 => 4 trans 0 => 4, 
                3 => 8 trans 0 => 5
        );

        for (src, tgt) in [(0, 0), (3, 3), (4, 4), (5, 2)] {
            assert_eq!(trans(src), tgt);  
        } 
    }
 }

/// Macro for generating default impls for traits generic to complete/streaning nyaaa
macro_rules! abnf_core {
    {
        $(#[$meta:meta])*
        $vis:vis trait $trait_name:ident = $complete_or_streaming:ident
    } => {
        $(#[$meta])*
        $vis trait $trait_name {
            abnf_core!{@litsyms $complete_or_streaming
                /// rule ALPHA - r_ prefix
                r_alpha is_alpha;
                r_bit is_bit;
                r_char is_char;
                r_cr is_cr;
                r_ctl is_ctl;
                r_digit is_digit;
                r_dquote is_dquote;
                r_htab is_htab;
                r_lf is_lf;
                r_octet is_octet;
                r_sp is_sp;
                r_vchar is_vchar
            }

            abnf_core!{@fndecls $complete_or_streaming
                fn r_hexdigit(i) {
                    alt((
                        Self::r_digit,
                        single_input_matches(|&v| ranges!(v in b'A' ~ b'F'))
                    ))(i)
                }

                fn r_crlf(i) {
                    recognize(pair(Self::r_cr, Self::r_lf))(i)
                }

                /// ASCII Whitespace Rule (SP/HTAB)
                fn r_wsp(i) {
                    alt((Self::r_sp, Self::r_htab))(i)
                }

                /// Whitespace lines - *((WSP)/(CRLF WSP))
                ///
                /// WARNING: As a [nom::multi::many0] rule, using this rule in any other rule that consumes
                /// zero input is a risk in the case that containing rule itself is placed in a []
                fn r_lwsp(i) {
                    recognize(many0_count(alt((Self::r_wsp, recognize(pair(Self::r_crlf, Self::r_wsp))))))(i)
                }
            }
        }
    };


    {@fndecls $complete_or_streaming:ident $($(#[$meta:meta])* fn $fnname:ident ($inarg:ident) { $($contents:expr)? })* } => {$(
        $(#[$meta])*
        #[allow(unused_imports)]
        fn $fnname<'i, E: ::nom::error::ParseError<&'i [u8]>>($inarg : &'i [u8]) -> ::nom::IResult<&'i [u8], &'i [u8], E> {
            use $crate::nom_byte_utils::$complete_or_streaming::single_input_matches;
            use ::nom::{combinator::recognize, branch::alt, sequence::{tuple, pair}, multi::many0_count};
            $($contents)?
        }
    )*};

    {@litsyms $complete_or_streaming:ident $($(#[$meta:meta])* $traitfnname:ident $bytefnname:ident);*} => {
        $(
            $(#[$meta])*
            fn $traitfnname<'i, E: ::nom::error::ParseError<&'i [u8]>>(i: &'i [u8]) -> nom::IResult<&'i [u8], &'i [u8], E> {
                $crate::nom_byte_utils::$complete_or_streaming::single_input_matches(
                    |&v| $crate::bytefunctions::$bytefnname(v)
                )(i)
            }
        )*
    }
}

/// Functions for testing bytes for matching literal core rules (not composed of other
/// rules, as those must be able to be overwritten by [streaming::ABNFCoreRules] and
/// [complete::ABNFCoreRules] implementors.
pub mod bytefunctions {
    /// If the byte matches the core ALPHA rule.
    pub const fn is_alpha(charbyte: u8) -> bool {
        ranges!(charbyte in b'A' ~ b'Z', b'a' ~ b'z')
    }

    /// If the byte matches the core BIT rule.
    pub const fn is_bit(charbyte: u8) -> bool {
        charbyte == b'0' || charbyte == b'1'
    }

    /// If the byte matches the CHAR rule - i.e. is it a nonnull
    /// ascii character
    pub const fn is_char(charbyte: u8) -> bool {
        ranges!(charbyte in ~ 0x7F)
    }

    /// If the byte matches the CR (carriage return) rule
    pub const fn is_cr(charbyte: u8) -> bool {
        charbyte == 0x0D
    }

    /// If the character is an ASCII control character
    pub const fn is_ctl(charbyte: u8) -> bool {
        ranges!(charbyte in ~ 0x1F) || charbyte == 0x7F
    }

    /// If the character is a 0-9 digit
    pub const fn is_digit(charbyte: u8) -> bool {
        ranges!(charbyte in b'0' ~ b'9')
    }

    /// If the character is a doublequote character
    pub const fn is_dquote(charbyte: u8) -> bool {
        charbyte == b'"'
    }

    /// If the character is the horizontal tab character
    pub const fn is_htab(charbyte: u8) -> bool {
        charbyte == 0x09
    }

    /// If the character is the linefeed character
    pub const fn is_lf(charbyte: u8) -> bool {
        charbyte == 0x0A
    }

    /// If the character is an OCTET/byte (always true)
    pub const fn is_octet(_: u8) -> bool {
        true
    }

    /// If the character is SP (presumably space)
    pub const fn is_sp(charbyte: u8) -> bool {
        charbyte == 0x20
    }

    /// If the character is VCHAR (visible/printing) ascii character
    pub const fn is_vchar(charbyte: u8) -> bool {
        ranges!(charbyte in 0x21 ~ 0x7E)
    }
}

/// Contains [nom] streaming parser ABNF core rules information.
pub mod streaming {

    abnf_core! {
    /// Trait for the ABNF core rules, in nom streaming mode
    ///
    /// Default implementations are the core rules as specified in
    /// [ABNF Core Rules](https://datatracker.ietf.org/doc/html/rfc5234)
    ///
    /// All rules overriden will cascade to rules that use them.
    pub trait ABNFCoreRules = streaming
    }

    /// Holds the default (unmodified) ABNF Core Rules (streaming nom parsing mode)
    pub struct DefaultABNFCoreRules;
    impl ABNFCoreRules for DefaultABNFCoreRules {}
}

/// Contains [nom] complete parser ABNF core rules information.
pub mod complete {
    abnf_core! {
    /// Trait for the ABNF core rules, in nom complete mode
    ///
    /// Default implementations are the core rules as specified in
    /// [ABNF Core Rules](https://datatracker.ietf.org/doc/html/rfc5234)
    ///
    /// All rules overriden will cascade to rules that use them.
    pub trait ABNFCoreRules = complete
    }

    /// Holds the default (unmodified) ABNF Core Rules (complete nom parsing mode)
    pub struct DefaultABNFCoreRules;
    impl ABNFCoreRules for DefaultABNFCoreRules {}
}

/// Module that provides [nom] combinators to allow writing parsers in a simple
/// manner based on any specification written with the [ABNF](https://datatracker.ietf.org/doc/html/rfc5234)
/// language-specification language, using other tools provided in this crate.
///
/// The means by which this is done is by providing aliases for various ABNF combining operators
/// such as / and <n>?*<m>? as nom combinators. By default, these functions simply return a slice
/// of the input string as their output - as with something like [nom::combinator::recognize].
///
/// Note of care - many ABNF patterns/languages contain rules that can be *zero length* - for
/// instance, the Linear Whitespace rule (LWSP). These rules will, on zero length matches, *break*
/// any [nom::multi::many0] or similar combinators because they check for zero-length input
/// consumption and error out to prevent infinite loops.
///
/// As such, be careful to minimise the amount of zero-length patterns and do NOT use zero length
/// patterns inside other potentially zero length pattern multipliers! It will break things!
/// Instead, try to use non-zero-length patterns in these sorts of multipliers.
pub mod abnf_rules {
    macro_rules! abnf_aliases{
        {$complete_or_streaming:ident} => {
            use nom::combinator::recognize;
        };
    }

    /// Complete-parser ABNF rules. See [crate::abnf_rules]
    pub mod complete {
        pub use crate::{ranges, translations};
    }

    /// Streaming-parser ABNF rules. See [crate::abnf_rules]
    pub mod streaming {
        pub use crate::{ranges, translations};
    }
}

//  Copyright © 2022 <sapient_cogbag at protonmail dot com>
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy of this software
//  and associated documentation files (the “Software”), to deal in the Software without
//  restriction, including without limitation the rights to use, copy, modify, merge, publish,
//  distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
//  Software is furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all copies or
//  substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
//  BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
//  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.}
